# Package Database

The package database is a simple text file [cts.txt](cts.txt) with the following format:

| pk3mame       | -options      | url   |
|:------------- |:------------- |:----- |
| packer_tacos.pk3 | -    | http://x1.devfull.de/packages/cts/packer_tacos.pk3 |

## Options

Following options are available and can be combined: (e.g *-r*, *-rmp*)

| Option | Description |
|:------ |:----------- |
| -0     | do not write a mapinfo file (use the one inside the pke)
| -b     | enable **crylink** as start weapon
| -h     | enable **hagar** as start weapon
| -m     | enable **mortar** as start weapon
| -r     | enable **rocketlauncher** as start weapon
| -w     | use weaponarena (*g_weaponarena all*)
| -f     | use *physicsXDFLight.cfg* instead of default XDF physics [1]
| -t     | enable *haste* physics (*sv_maxspeed 400*, *sv_maxairspeed 400*)
| -g     | reduce gravity (*sv_gravity 600*)
| -p     | disable playerclips (*g_playerclip_collisions 0*)
| -c     | reduce crouch height
| -e     | further reduce crouch height (compared to -c)
| -s     | reduce player size
| -d     | disable door patch [2]
| -j     | apply teleporter fix [3]
| -k     | enforce slick (*sv_friction 0*) for complete map
| -a     | disable ammo system (*g_use_ammunition 0*) **obsolete**
| -d     | disable self damage (*g_balance_selfdamagepercent 0*) **obsolete**
| -l     | set teleporter speed to 600 (*g_teleport_maxspeed 600*) **obsolete**

[1]: XDF uses [physicsXDF.cfg](https://gitlab.com/xonotic/xonotic-data.pk3dir/blob/master/physicsXDF.cfg) by default.
For compatibility reasons [physicsXDFLight.cfg](https://gitlab.com/xonotic/xonotic-data.pk3dir/blob/master/physicsXDFLight.cfg) exists, e.g. for Nexrun maps.

[2]: As many players play at the same time doors stay opened once opened to keep equal chances.
This behaviour can be disabled (doors close again) using this option.

[3]: Sometimes map have issues with teleporter targets and players are stuck after being teleported.
To avoid this issue this option can be used. (It shifts the teleport target by 10 qu.)

### Modifed player sizes

Modified player sizes cause prediction errors. Thus use these options only if required and only those that are required.

The playersize changes are as follows:

* **-s** ... reduce player size:
```
settemp_for_type all sv_player_mins "-14 -14 -24"
settemp_for_type all sv_player_maxs "14 14 43"
ettemp_for_type all sv_player_crouch_mins "-14 -14 -24"
settemp_for_type all sv_player_crouch_maxs "14 14 23"
```

* **-c** ... reduce crouch size:
```
settemp_for_type all sv_player_crouch_maxs "16 16 23"
```

* **-e** ... further reduce crouch size:
```
settemp_for_type all sv_player_crouch_maxs "16 16 19"
```


